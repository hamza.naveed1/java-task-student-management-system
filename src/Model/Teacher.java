package Model;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class Teacher extends User {

    public Teacher(String name, String email, String role) {
        super(name, email, role);
        userMap.put(this.email, this);
    }

    @Override
    public void view() {
        System.out.println("proceeding as Teacher");
        System.out.println("Please select an action:\n1.View all the marks of a specific student\n2.Update a specific student marks\n3.Return to the previous menu");
    }

    @Override
    public String toString() {
        return "Model.Teacher{" +
                "teachSub='" + "teachSub" + '\'' +
                ", name='" + name + '\'' +
                ", email='" + email + '\'' +
                ", role='" + role + '\'' +
                '}';
    }

    @Override
    public void actions(int i) {
        Scanner sc1 = new Scanner(System.in);
        Scanner sc2 = new Scanner(System.in);
        Scanner sc3 = new Scanner(System.in);
        if (i == 2) {
            System.out.println("You are updating the marks of a student");
            System.out.println("please input the students Email");
            String name = sc1.nextLine();
            System.out.println("please enter the subject");
            String sub = sc2.nextLine();
            System.out.println("please enter the marks");
            int marks = sc3.nextInt();
            updateMarks(User.userMap.get(name), sub, marks);
        } else if (i == 1) {
            System.out.println("please input the students Email");
            String name = sc1.nextLine();
            showStudentProgress((Student) User.userMap.get(name));
        }
    }

    public void updateMarks(User student, String sub, int marks) {
        Student st = (Student) student;
        List<Integer> change=new ArrayList<>();
        for (int i=0;i<st.subjects.size();i++){
            if (st.subjects.get(i).getSubName().equals(sub)
                    && st.subjects.get(i).teacher.getEmail().equals(this.email)){
                for (int j = 0; j < st.getSubjects().size(); j++) {
                if (st.getSubjects().get(j).getSubName().equals(sub)) {
                    st.getSubjects().get(j).setMarks(marks);
                }
            }change.add(1);
            } else {
            change.add(0);
            }
        }
        if (change.contains(1)){
            System.out.println("Marks successfully updated");
        }else {
            System.out.println("you are not authorized to change the marks of the subject you don't teach ");
        }
    }

    public void showStudentProgress(Student st) {
        System.out.println("Name = " + st.name);
        int i = 1;
        for (Subject s : st.subjects) {
            System.out.println(i + "." + s);
            i += 1;
        }
    }
}
