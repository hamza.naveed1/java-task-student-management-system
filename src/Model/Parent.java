package Model;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class Parent extends User{
    List<Student> children=new ArrayList<>();


    public Parent(String name, String email, String role) {
        super(name, email, role);
        userMap.put(this.email,this);
    }

    @Override
    public void view() {
        System.out.println("proceeding as Parent");
        System.out.println("Please select an action:\n1.Add children\n2.View your child's marks\n3.view your children in the school\n4.Return to the previous menu");
    }


    public void viewChildren(){
        System.out.println("Children of "+this.name+" in the school are:");
        for (int i=0;i<children.size();i++){
            System.out.println(i+1+"."+children.get(i).getName());
        }
    }

    public void viewChildProgress() {
        System.out.println("==============================================");
        System.out.println("Marks of the Children of " + this.name + " are:");
        for (Student child : children) {
            System.out.println("\n");
            child.showMarks();
        }
    }

    @Override
    public void actions(int i) {
        if (i==1){
            Scanner sc=new Scanner(System.in);
            System.out.println("please enter the student's email");
            String email=sc.nextLine();
            this.addChild((Student) User.userMap.get(email));
        } else if (i==2) {
            this.viewChildProgress();
        } else if (i==3) {
            this.viewChildren();
        }

    }

    public List<Student> getChildren() {return children;}

    public void addChild(Student st) {
        children.add(st);
    }

    @Override
    public String toString() {
        return "Model.Parent{" +
                "children=" + children +
                ", name='" + name + '\'' +
                ", email='" + email + '\'' +
                ", role='" + role + '\'' +
                '}';
    }
}
