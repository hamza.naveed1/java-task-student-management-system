package Model;

import java.util.*;

public class Student extends User{
    List<Subject> subjects =new ArrayList<>();


    public Student(String name, String email, String role) {
        super(name, email, role);
        userMap.put(this.email,this);
    }

    @Override
    public void view() {
        System.out.println("proceeding as Student");
        System.out.println("Please select an action:\n1.View your marks\n2.Add a new Subject\n3.Return to the previous menu");
    }

    @Override
    public void actions(int i) {
        if (i==1){
            this.showMarks();
        } else if (i==2) {
            Scanner sc=new Scanner(System.in);
            System.out.println("please enter the subject you want to enter");
            String name=sc.nextLine();
            this.addSubject(name);
        }
    }

    public List<Subject> getSubjects() {return subjects;}

    public void setSubjects(Subject sub) {subjects.add(sub);}

    public void showMarks(){
        System.out.println("Name = "+this.name);
        int i=1;
        for (Subject s:this.subjects){
            System.out.println(i+"."+s);
            i+=1;
        }
    }
    public void addSubject(String sub){
        Subject subject=new Subject(sub);
        subjects.add(subject);
    }


}
