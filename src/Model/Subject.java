package Model;

public class Subject {
    private String subName;
    private int marks;
    public Teacher teacher;


    public Subject(String subName){
        this.subName=subName;
        this.marks=0;

        switch (subName) {
            case "p1" -> {
                this.teacher = (Teacher) User.userMap.get("p1@gmail.com");
            }
            case "p2" -> {
                this.teacher = (Teacher) User.userMap.get("p2@gmail.com");
            }
            case "mechanics" -> {
                this.teacher = (Teacher) User.userMap.get("tb@gmail.com");
            }
        }
    }
    public String getSubName() {return subName;}


    public void setSubName(String subName) {this.subName = subName;}

    public int getMarks() {return marks;}

    public void setMarks(int marks) {this.marks = marks;}

    @Override
    public String toString() {
        return "Subject='" + subName +
                ", marks=" + marks ;
    }
}
