package Model;

import java.util.HashMap;
import java.util.Map;

public abstract class User {
    String name;
    String email;
    String role;
    //userMap made static so it can be accessed in the school class without creating a USER object
    public static Map<String, User> userMap = new HashMap<>();

    public User(String name, String email, String role) {
        this.name = name;
        this.email = email;
        this.role = role;
    }

    public abstract void view() ;


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    @Override
    public String toString() {
        return "Model.User{" +
                "name='" + name + '\'' +
                ", email='" + email + '\'' +
                ", role='" + role + '\'' +
                '}';
    }

    public abstract void actions(int i);

}