package Controller;

import Model.User;

import java.util.Scanner;

public class School {

    public void mainManuView(){
        System.out.println("===========================================");
        System.out.println("Please enter your Email:");
        System.out.println("===========================================");
    }
    public void returnOrRepeat(){
        System.out.println("please REPEAT to return to the previous menu or EXIT to exit");
    }


    public void mainApplication(){
        Scanner sc10=new Scanner(System.in);
        Scanner sc20=new Scanner(System.in);
        mainManuView();
        int choice = 0;
        while (choice != 4) {
            Scanner sc = new Scanner(System.in);
            String input = sc.nextLine();
            int option =0;

            //if the input is repeat or exit the userMap.get(input) will be null
            if (User.userMap.get(input)!=null) {
                String userRole= User.userMap.get(input).getRole();
                switch (userRole) {
                    case "parent" -> option = 1;
                    case "student" -> option = 2;
                    case "teacher" -> option = 3;
                }
                //calls the appropriate  view according the input from the user
                User.userMap.get(input).view();
            }else {
                if (input.contains("exit")||input.contains("EXIT")) {
                    option=4;
                }else if (input.contains("repeat")||input.contains("REPEAT")) {
                    option=5;
                }else {
                    System.out.println("please Enter a registered email address");
                }
            }

            switch (option) {
                //PARENT
                case 1 -> {
                    //will show the view according to the role of the user
                    int opt = sc.nextInt();
                    switch (opt) {
                        case 1 -> {
                            try {
                                int answer;
                                User.userMap.get(input).actions(opt);
                                System.out.println("please press 1 if you would like to add more children or 2 otherwise");
                                answer = sc20.nextInt();
                                while (answer != 2) {
                                    User.userMap.get(input).actions(opt);
                                    System.out.println("please press 1 if you would like to add more children or 2 otherwise");
                                    int ans = sc10.nextInt();
                                    if (ans == 2) {
                                        answer = 2;
                                    } else answer = 1;

                                }
                                returnOrRepeat();
                            }catch (Exception e){
                                System.out.println(e);
                                returnOrRepeat();
                            }

                        }
                        case 2 ,3-> {
                            //Model.User.userMap.get(input) returns the user object which will call actions()
                            //calls action() from user and passes the opt as parameter
                            User.userMap.get(input).actions(opt);
                            returnOrRepeat();
                        }
                        case 4 -> {
                            mainManuView();
                        }
                    }
                }
                //STUDENT
                case 2 -> {
                    int opt = sc.nextInt();
                    switch (opt) {
                        case 1 -> {
                            User.userMap.get(input).actions(opt);
                            returnOrRepeat();
                        }
                        case 2 -> {
                            try {
                                int answer;
                                User.userMap.get(input).actions(opt);
                                System.out.println("please press 1 if you would like to add more subjects or 2 otherwise");
                                answer = sc20.nextInt();
                                while (answer != 2) {
                                    User.userMap.get(input).actions(opt);
                                    System.out.println("please press 1 if you would like to add more subjects or 2 otherwise");
                                    int ans = sc10.nextInt();
                                    if (ans == 2) {
                                        answer = 2;
                                    } else answer = 1;

                                }
                                returnOrRepeat();
                            }catch (Exception e){
                                System.out.println(e);
                                returnOrRepeat();
                            }

                        }
                        case 3 -> {
                            mainManuView();
                        }
                    }
                }
                //TEACHER
                case 3 -> {
                    int opt = sc.nextInt();
                    switch (opt) {
                        case 1, 2 ,4,5-> {
                            User.userMap.get(input).actions(opt);
                            returnOrRepeat();
                        }
                        case 3 -> {
                            mainManuView();
                        }
                    }
                }
                case 4 -> choice = 4;
                case 5 -> {
                    mainManuView();
                }
            }
        }
    }
}
