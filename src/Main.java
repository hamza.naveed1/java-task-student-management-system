import Controller.School;
import Model.*;

public class Main {
    public static void main(String[] args) {
        School mySchool = new School();

        User teacher = new Teacher("Tahir Abbas", "tb@gmail.com", "teacher");
        User teacher1 = new Teacher("Masood", "p1@gmail.com", "teacher");
        User teacher2 = new Teacher("Ahmed", "p2@gmail.com", "teacher");

        User st = new Student("Hamza Naveed", "ham", "student");
        User st1 = new Student("ABC Naveed", "a", "student");
        User st3 = new Student("CDF Naveed", "cdf@gmail.com", "student");

        User parent1 = new Parent("Naveed Tabassum", "nav", "parent");

        mySchool.mainApplication();
    }
}